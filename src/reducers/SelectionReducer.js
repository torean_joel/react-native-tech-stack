export default (state = null, action) => {
  //use a switch that will be used to check for the action types
  switch (action.type) {
    case 'select_library':
      return action.payload;
    default:
      return state;
  }
};