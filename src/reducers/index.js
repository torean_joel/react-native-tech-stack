import { combineReducers } from 'redux';
//import custom reducers here
import Libraries from './LibraryReducer';
import SelectionReducer from './SelectionReducer';

//export and assign reducers to the store
export default combineReducers({
  libraries: Libraries,
  selectionReducerId: SelectionReducer
});