import React from 'react';
import { View, Text, TouchableWithoutFeedback, LayoutAnimation, UIManager, Platform } from 'react-native';
import { connect } from 'react-redux';
import { CardSection } from './common';
import * as actions from '../actions';

if (Platform.OS === 'android') UIManager.setLayoutAnimationEnabledExperimental(true)

class ListItem extends React.Component {
  componentWillUpdate() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
  }
  //method to show relevant vie based on the selected library
  renderMoreDetails() {
    if(this.props.expanded) {
      return (
        <CardSection>
          <Text style={styles.description}> { this.props.library.description }</Text>
        </CardSection>
      )
    }
  }

  render() {
    return (
      <TouchableWithoutFeedback 
        onPress={() => this.props.selectLibrary(this.props.library.id)}>
        <View>
          <CardSection>
            <Text style={styles.title}> { this.props.library.title }</Text>
          </CardSection>
          { this.renderMoreDetails() }
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = {
  title: {
    alignSelf: 'center',
    fontSize: 15,
    paddingLeft: 10
  },
  description: {
    flex: 1,
    paddingLeft: 10,
    textAlign: 'justify'
  }
}

//get the store if you want to base logic off the state
const mapStateToProps = (state, ownProps) => {
  //this is good, remove code from components
  //check the state of the reducer return state after the user presses and updates id
  //against the id that is passed from the parent to this component (ownProps === this.props)
  // 2 arguments, first state, second, the current componenets props
  const expanded = state.selectionReducerId === ownProps.library.id
  return { expanded }
}

export default connect(mapStateToProps, actions)(ListItem);
