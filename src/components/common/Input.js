import React from 'react';
import { View, Text, TextInput } from 'react-native';

const Input = (props) => {
  return (
    <View style={ styles.containerStyle }>
      <Text style={ styles.labelStyle }>{ props.label }: </Text>
      <TextInput
        autoCorrect={ false }
        secureTextEntry={ props.secureTextEntry }
        style={ styles.inputStyle }
        value={ props.value }
        placeholder={props.placeholder}
        onChangeText={ props.onChangeText } />
    </View>
  )
}

const styles = {
  inputStyle: {
    color: '#000',
    paddingRight: 5,
    paddingLeft: 5,
    paddingBottom: 10,
    fontSize: 15,
    lineHeight: 23,
    flex: 2,
  },
  labelStyle: {
    fontSize: 18,
    flex: 1,
  },
  containerStyle: {
    paddingLeft: 10,
    paddingRight: 10,
    height: 40,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  }
}

export { Input }