import React from 'react';
import { View, Dimensions } from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
//custom imports here
import { Header } from './src/components/common';
import reducers from './src/reducers';
import LibraryList from './src/components/LibraryList';

//dementions of the current device's screen
var winSize = Dimensions.get('window');

const App = () => {
  return (
    <Provider store={createStore(reducers)} >
      <View style={styles.container}>
        <Header headerTitle='Tech Stack' />
        <LibraryList />
      </View>
    </Provider>
  )
}

const styles = {
  container: {
    flex: 1,
    height: winSize.height
  }
}

export default App;